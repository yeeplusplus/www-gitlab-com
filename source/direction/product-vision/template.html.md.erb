---
layout: markdown_page
title: "Product Vision"
extra_css:
  - product-vision.css
---

- TOC
{:toc}

This is the vision for the end of 2018.

GitLab reimagines the scope of DevOps tooling to include developers, operations,
and security teams in [one single application](/direction/#single-application).
This dramatically reduces friction, increases collaboration, and drives a
competitive advantage. Doing away with context switching and having all of the
necessary information in one place closes the loop and enables a better
understanding of each team's needs.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/RmSTLGnEmpQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

- [Prototype Demo Slides](https://docs.google.com/presentation/d/19dZ1Y4us11B_96YoXvgQL4aBXPy2iNYRId0vmTulnnQ)
- [Interactive Framer Prototype](https://framer.cloud/UaofH/index.html) - click the header to move to the next page, click the left sidebar to move back

To make our vision a reality, we're working on a number of new features and
improving on existing ones. This page describes the Product Vision for 2018.
Internally, we're intending on implementing everything in here before the 2018
summit, scheduled for Aug 23, 2018. Of course that's incredibly
aggressive, so as a customer or prospect, please don't count on that date. Some
of these might only get to a minimal, or even just a demoable state by then.

Many of the issues describe development of an n-tier web app, but could equally
be applied to an iOS app, Ruby gem, static website, or other type of project.

Grouping is based on stages in the [DevOps
Toolchain](https://en.wikipedia.org/wiki/DevOps_toolchain) and our [product
categories](/handbook/product/categories/), including several new ones for 2018. 
Also see our [blog post](/2017/10/11/from-dev-to-devops) on the complete
DevOps vision. Or see the [open issues for `Product Vision
2018`](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Product+Vision+2018).

![DevOps Lifecycle](devops-loop.png)

![New in 2018](product-categories.png)

## Plan

The Plan stage is about capturing ideas, organizing issues and epics,
scheduling work, and communicating plans.

| Jira        | Epics           |
| ------------- |:-------------:| -----:|
| ![Jira in Menu](plan-jira-in-menu.png) | ![Epic Roadmaps](plan-epic-roadmaps.png) |

### Chat

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Mattermost](https://docs.gitlab.com/omnibus/gitlab-mattermost/)

### Issue Management

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Issue tracker](https://docs.gitlab.com/ee/user/project/issues/)<br>
<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Issue board](https://docs.gitlab.com/ee/user/project/issue_board.html)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Jira development panel integration](https://gitlab.com/groups/gitlab-org/-/epics/73)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Import Jira issues to GitLab issues](https://gitlab.com/groups/gitlab-org/-/epics/10)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Real-time editing of issues and merge requests](https://gitlab.com/groups/gitlab-org/-/epics/52)<br>

### Portfolio Management

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Epics](https://docs.gitlab.com/ee/user/group/epics/)<br>
<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Roadmap](https://docs.gitlab.com/ee/user/group/roadmap/)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Capacity planning and epic tracking in roadmap](https://gitlab.com/groups/gitlab-org/-/epics/76)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Group milestone as a release (in addition to a sprint)](https://gitlab.com/groups/gitlab-org/-/epics/69)<br>

### Cycle Analytics

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Cycle Analytics](https://docs.gitlab.com/ee/user/project/cycle_analytics.html)<br>

### Conversational Development Index

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Conversational Development Index](https://docs.gitlab.com/ee/user/admin_area/monitoring/convdev.html)<br>

## Create

The Create stage is about creating, collaborating, and managing content; often
source code, but increasingly other content such as design files.

| Bulk review      | Web IDE           |
| ------------- |:-------------:| -----:|
| ![Preview Drafts](create-preview-drafts-list.png) | ![Web IDE](create-web-ide.png) |

### Version Control

<%= product_vision["repository"] %>

### Code Review
<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Merge request](https://docs.gitlab.com/ee/user/project/merge_requests/)<br>
<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Image discussion in merge request diff](https://docs.gitlab.com/ee/user/discussions/#image-discussions)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Batch comments on merge request](https://gitlab.com/groups/gitlab-org/-/epics/23)<br>

### IDE

<%= product_vision["web ide"] %>

## Verify

The Verify stage is all about making sure that your code does what you expected
it to do, meets quality standards, and is secure; all via automated testing. We
bring best practices from top development teams, and make them the easy, default
way to work. But of course you can build on those defaults and customize as
needed for your company.

### Continuous Integration (CI)

GitLab CI provides an explicit `build` stage and the concept of build artifacts,
but we might need to separate out the build artifacts from test artifacts. For
example, you might want your test runner to create a JUnit-style output file
which is available for external consumption, but not included in the build image
sent to production. Creation of an explicit build aligns well with Docker where
the result of the build stage is a Docker image which is stored in a registry
and later pulled for testing and deployment.

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Browser testing with Selenium](https://gitlab.com/groups/gitlab-org/-/epics/98) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Enhanced Auto DevOps](https://gitlab.com/groups/gitlab-org/-/epics/121)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Improved Code Quality reporting](https://gitlab.com/groups/gitlab-org/-/epics/104) <kbd>Starter</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Improving security products features](https://gitlab.com/groups/gitlab-org/-/epics/117) <kbd>Ultimate</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Manage code coverage reports](https://gitlab.com/groups/gitlab-org/-/epics/100) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Flaky tests management](https://gitlab.com/groups/gitlab-org/-/epics/99) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Improve speed of CI](https://gitlab.com/groups/gitlab-org/-/epics/130)<br>

## Package

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Container Registry](https://docs.gitlab.com/ee/user/project/container_registry.html)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Binary Repository](https://gitlab.com/groups/gitlab-org/-/epics/86) <kbd>Premium</kbd><br>

## Release

### Continuous Delivery (CD) / Release Automation

A key part of CD is being able to deploy. We currently have the ability via
scripts in the `deploy` stage in `.gitlab-ci.yml`. We will go further.

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Environments](https://docs.gitlab.com/ee/ci/environments.html)<br>
<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Deployment history](https://docs.gitlab.com/ee/ci/environments.html#viewing-the-deployment-history-of-an-environment)<br>
<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Deploy boards](https://docs.gitlab.com/ee/user/project/deploy_boards.html) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Canary deployments](https://docs.gitlab.com/ee/user/project/canary_deployments.html) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Improved Environments view](https://gitlab.com/groups/gitlab-org/-/epics/109)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [First class Kubernetes integration](https://gitlab.com/groups/gitlab-org/-/epics/87)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Release train](https://gitlab.com/groups/gitlab-org/-/epics/101) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Incremental rollouts](https://gitlab.com/groups/gitlab-org/-/epics/102) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Automatic rollback of deployments if error rate is too high](https://gitlab.com/groups/gitlab-org/-/epics/103) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Group-level Environments page](https://gitlab.com/groups/gitlab-org/-/epics/110) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Monitor pods status in deploy boards after deployment](https://gitlab.com/groups/gitlab-org/-/epics/131) <kbd>Premium</kbd><br>

## Configure

### Application Control Panel

<i class="vision-item fa fa-check-square-o" aria-hidden="true"></i> [Secret variables](https://docs.gitlab.com/ee/ci/variables/#secret-variables)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Drive Auto DevOps deployment strategies with variables](https://gitlab.com/groups/gitlab-org/-/epics/106)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Drive Auto DevOps database support with variables](https://gitlab.com/groups/gitlab-org/-/epics/127)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Disable Auto DevOps jobs with variables](https://gitlab.com/groups/gitlab-org/-/epics/128)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Scale a deployment from the Environments page](https://gitlab.com/groups/gitlab-org/-/epics/108)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Autoscaling deployments based on CPU usage](https://gitlab.com/groups/gitlab-org/-/epics/112) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Pause idle deployments](https://gitlab.com/groups/gitlab-org/-/epics/113) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Allow easy configuration of Auto DevOps from settings page](https://gitlab.com/groups/gitlab-org/-/epics/107)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Automatically detect and enable services when deploying applications](https://gitlab.com/groups/gitlab-org/-/epics/129)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [GitLab PaaS](https://gitlab.com/groups/gitlab-org/-/epics/111) <kbd>Premium</kbd><br>

### Infrastructure Configuration

<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Group-level Kubernetes cluster](https://gitlab.com/groups/gitlab-org/-/epics/114)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Configure GKE clusters](https://gitlab.com/groups/gitlab-org/-/epics/115) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Monitoring of Kubernetes cluster](https://gitlab.com/gitlab-org/gitlab-ce/issues/27890) <kbd>Ultimate</kbd><br>

### Operations

<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Operator role](https://gitlab.com/groups/gitlab-org/-/epics/116) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Error Tracking](https://gitlab.com/gitlab-org/gitlab-ce/issues/38092) <kbd>Ultimate</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Continuous dependency security/update checking](https://gitlab.com/groups/gitlab-org/-/epics/133) <kbd>Ultimate</kbd><br>

### Feature Management

There's a big benefit to decoupling deployment of code from delivery of a
feature, mostly using feature flags. Continuous integration helps improve the
speed of development, but feature flags take it to another level, giving you the
confidence to integrate code even more often while providing a gradual and
granular method for delivery.

<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Feature Management](https://gitlab.com/groups/gitlab-org/-/epics/105) <kbd>Premium</kbd><br>

### ChatOps

<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [ChatOps Improvements](https://gitlab.com/groups/gitlab-org/-/epics/74)<br>

## Monitor

Performance is a critical aspect of the user experience, and ensuring your
application is responsive and available is everyone's responsibility. We want to
help address this need for development teams, by integrating key performance
analytics and feedback into the tool developers already use every day.

Going further, we want you to be able to track changes in your production
infrastructure, availability, and uptime; error/exception tracking; and
aggregated logs.

As part of our commitment to performance we are also deeply instrumenting GitLab
itself, enabling our team to improve GitLab peformance and for customers to more
easily manage their deployments.

### Application Performance Monitoring (APM)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-check-square-o" aria-hidden="true"></i> [Prometheus monitoring](https://docs.gitlab.com/ee/ci/environments.html#monitoring-environments)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-check-square-o" aria-hidden="true"></i> [Custom Metrics](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#adding-additional-metrics) <kbd>Premium</kbd><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [GitLab Tracing](https://gitlab.com/groups/gitlab-org/-/epics/89) <kbd>Ultimate</kbd><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Load/Performance impact of merge request](https://gitlab.com/groups/gitlab-org/-/epics/118) <kbd>Premium</kbd><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [SLO Alerts](https://gitlab.com/gitlab-org/gitlab-ee/issues/5158) <kbd>Ultimate</kbd><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Anomaly Detection](https://gitlab.com/groups/gitlab-org/-/epics/119) <kbd>Ultimate</kbd><br>

### Production Monitoring

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Operations Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/141) <kbd>Ultimate</kbd><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Production Scripted Testing](https://gitlab.com/gitlab-org/gitlab-ee/issues/3554) <kbd>Ultimate</kbd><br>

### Error Tracking

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Error Tracking](https://gitlab.com/gitlab-org/gitlab-ee/issues/5459) <kbd>Ultimate</kbd><br>

### Logging

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Kubernetes Pod Logs](https://gitlab.com/groups/gitlab-org/-/epics/90) <kbd>Ultimate</kbd><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Application Logs](https://gitlab.com/gitlab-org/gitlab-ee/issues/3711) <kbd>Ultimate</kbd><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-square-o" aria-hidden="true"></i> [Application Log Alerts](https://gitlab.com/gitlab-org/gitlab-ee/issues/3626) <kbd>Ultimate</kbd><br>

## Performance

*Not a stage, but part of our 2018 plan*

<i class="vision-item fa fa-square-o" aria-hidden="true"></i> Push to Geo secondary (proxied to primary) <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> Automatic DR <kbd>Premium</kbd><br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> Improve speed index https://about.gitlab.com/handbook/engineering/performance/#speed-index<br>
<%= product_vision["performance"] %>

## Distribution

*Not a stage, but part of our 2018 plan*

GitLab is the engine that powers many companies' software businesses so it is
important to ensure it is as easy as possible to deploy, maintain, and stay up
to date.

Today we have a mature and easy to use Omnibus-based build system, which is the
foundation for nearly all methods of deploying GitLab. It includes everything a
customer needs to run GitLab all in a single package, and is great for
installing on virtual machines or real hardware. We are committed to making our
package easier to work with, highly available, as well as offering automated
deployments on cloud providers like AWS.

We also want GitLab to be the best cloud native development tool, and offering a
great cloud native deployment is a key part of that. We are focused on offering
a flexible and scalable container-based deployment on Kubernetes, by using
enterprise grade Helm Charts.

### Omnibus

<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Instant SSL with Let's Encrypt](https://gitlab.com/groups/gitlab-org/-/epics/142)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [GitLab Web Admin Portal](https://gitlab.com/gitlab-org/gitlab-ce/issues/37674)<br>

### Cloud Native Installation

<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Automated GitLab Deployment on EKS/AWS](https://gitlab.com/groups/gitlab-org/-/epics/142)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Automated GitLab Deployment on GKE/GCP](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2437)<br>
<i class="vision-item fa fa-square-o" aria-hidden="true"></i> [Cloud native GitLab Helm Chart (GA)](https://gitlab.com/charts/helm.gitlab.io)<br>
