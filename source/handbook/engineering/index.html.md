---
layout: markdown_page
title: "Engineering"
---

## Communication<a name="reach-engineering"></a>

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce); please use confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/?redir=%2Farchives%2Fdevelopment); please use the `#development`, `#vpe`, `#frontend`, `#infrastructure`, `#ci-cd`, and `#support` chat channels for questions that don't seem appropriate to use the issue tracker or the internal email address for.

## On this page
{:.no_toc}

- TOC
{:toc}

## Other Related Pages

- [Developer onboarding](/handbook/developer-onboarding)
- [Engineering Career Development](/handbook/engineering/career-development)
- [Engineering Workflow](/handbook/engineering/workflow)
- [Frequently Used Projects](/handbook/engineering/projects)
- [Issue Triage Policies](/handbook/engineering/issue-triage)
- [Critical Security Release Process](/handbook/engineering/critical-release-process)
- [Performance of GitLab](/handbook/engineering/performance)
- [Monitoring of GitLab.com](/handbook/infrastructure/monitoring)
- [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)
- [Geo development](/handbook/engineering/geo/)
- [Contributing to Golang projects](/courses/dev-101)

## GitLab Repositories

GitLab consists of many subprojects. A curated list of GitLab Repositories can be found at the [GitLab Engineering Projects](/handbook/engineering/projects) page.

When adding a repository please follow these steps:
1. Ensure that the project is under the [gitlab-org](https://gitlab.com/gitlab-org) namespace for anything related to the application or under the [gitlab-com](https://gitlab.com/gitlab-com) namespace for anything strictly company related.
1. [Add the project to the list of GitLab Repositories](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/projects.md)
1. Add an MIT license to the repository. It is easiest to simply copy-paste the [MIT License](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/LICENSE) verbatim from the `gitlab-ce` repo.
1. Add a section titled "Developer Certificate of Origin and License" to `CONTRIBUTING.md` in the repository. It is easiest to simply copy-paste the [DCO + License section](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#developer-certificate-of-origin-license) verbatim from the `gitlab-ce` repo.
1. Add any further relevant details to the Contribution Guide. See [Contribution Example](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).
1. Add a link to `CONTRIBUTING.md` from the project's `README.md`

## Engineering Teams

* [Support](/handbook/support)
* [Infrastructure](/handbook/infrastructure)
  * [Database](/handbook/infrastructure/database)
  * [Gitaly](/handbook/infrastructure/gitaly)
  * [Production](/handbook/infrastructure/production)
* [Security](/handbook/engineering/security)
* [Backend](/handbook/backend)
  * [CI/CD](/handbook/backend#cicd)
  * [Discussion](/handbook/backend#discussion)
  * [Platform](/handbook/backend#platform)
  * [Monitoring](/handbook/backend#monitoring)
* [Frontend](/handbook/frontend)
* [Quality](/handbook/quality)
  * [Edge](/handbook/quality/edge)
* [UX](/handbook/ux)
* [Build](/handbook/build)

## Starting new teams

Our product offering is growing rapidly. Occasionally we start new teams. Backend teams should generally map to our [product categories](/handbook/product/categories/). Backend teams also map to [product managers](/handbook/product/).

A team needs certain skills and a minimum size to be successful. So we start a standlone team when we have the following:

* A specific feature set to make progress on for 1 year
* One of...
  * Fulltime experienced Engineering Manager & Senior Engineer
  * Inexperienced, new or interim Engineering Manager & fulltime Staff Engineer
* Two fulltime Intermediate (or higher) Engineers
* Two approved fulltime vacancies

Until this criteria is met the work should be scheduled on existing teams. If a new product manager starts before a team can be created they should work with a existing product managers to get work scheduled with their engineering teams.

## Collaboration

To maintain our rapid cadence of shipping a new release every month, we must keep the barrier low to getting things done. Since our team is distributed around the world and therefore working at different times, we need to work in parallel and asynchronously as much as possible.

That also means that if you are implementing a new feature, you should feel empowered to work on the entire stack if it is most efficient for you to do so.

## Code Quality and Standards

We need to maintain code quality and standards. It's very important that you are familiar with the [Development Guides] in general, and the ones that relates to your group in particular:

- [UX Guides](https://docs.gitlab.com/ee/development/ux_guide/index.html)
- [Backend Guides](https://docs.gitlab.com/ee/development/README.html#backend-howtos)
- [Frontend Guides](https://docs.gitlab.com/ee/development/fe_guide/index.html)
- [Database Guides](https://docs.gitlab.com/ee/development/README.html#databases)

[Development Guides]: https://docs.gitlab.com/ee/development/README.html

## Demos

The idea that [working software is the primary measure of progress](http://agilemanifesto.org/principles.html) is one of the principles of agile software development. Demoing gets more eyes on the project to uncover bugs and reveal ambiguities in the product requirements. It's also a transparent and lightweight way to provide status to the rest of the organization. Developers should demo at least once a week with product managers present. Demo meetings should be kept to 30 minutes or less. The emphasis should be on the product requirements or acceptance criteria and less on the implementation details.

## Canary Testing

GitLab makes use of a 'Canary' environment, a series of servers to test the GitLab code base on prior to production deployment. The Canary environment contains code functional elements like web and git servers while sharing data elements such as sidekiq, database, and file storage with production. This allows UX code and most application logic code to be user tested under real world scenarios before being deployed.

The Canary environment is available for usage by enabling a cookie attribute of `gitlab_canary` when accessing `gitlab.com`. All engineers are encouraged to enable this flag and perform normal daily activities in order to spot issues and problems before deployment to the production environment. In order to facilitate the enabling of the canary cookie, a short javascript snipped is provided below to bookmark that toggles between states.

```javascript
javascript:void((function(d){document.cookie='gitlab_canary=' + (document.cookie.indexOf('gitlab_canary=true') >= 0 ?  'false' : 'true') + ';domain=.gitlab.com;path=/;expires=' + new Date(Date.now() + 31536000000).toUTCString(); location.reload();})(document));
```

## Code Reviews

Code reviews are mandatory for every merge request, you should get familiar and follow our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html).

## Resources for Development
{: #resources}

When using any of the resources listed below, some rules apply:

* Consider the cost and whether anything can be done to reduce the cost.
* You can boot up as many machines as you need.
* It is your responsibility to clean up after yourself; if a machine is not used, remove it.
* If you observe any resource that is running for long periods of time, ask the person responsible whether the machine is still in use.
* Prepend your username to any resource you start. Eg. if your name is Jane Doe, name the resource `janedoe-machine-for-testing`.

### Google Cloud Platform (GCP)

Every team member has access to a common project on [Google Cloud Platform](https://console.cloud.google.com/). Please see the secure note with the name "Google Cloud Platform" in the shared vault in 1password for the credentials or further details on how to gain access.

Once in the console, you can spin up VM instances, Kubernetes clusters, etc. Please remove any resources that you are not using, since the company is [billed monthly](https://cloud.google.com/pricing/). If you are unable to create a resource due to quota limits, file an issue on the [Infrastructure issue tracker](https://gitlab.com/gitlab-com/infrastructure).

### Digital Ocean (DO)

Every team member has access to the [dev-resources project](https://gitlab.com/gitlab-com/dev-resources/) which allows everyone to create and delete machines on demand.

### Amazon Web Services (AWS)

In general, most team members do not have access to AWS accounts. In case you need an AWS resource, file an issue on the [Infrastructure issue tracker](https://gitlab.com/gitlab-com/infrastructure). Please supply the details on what type of access you need.

## DevOps Slack Channels

There are primarily two Slack channels which developers may be called upon to assist the production team
when something appears to be amiss with GitLab.com:

1. `#backend`: For backend-related issues (e.g. error 500s, high database load, etc.)
2. `#frontend`: For frontend-related issues (e.g. JavaScript errors, buttons not working, etc.)

Treat questions or requests from production team for immediate urgency with high priority.

## Developers on Support Team Rotation

See [the fix4all description](/handbook/engineering/fix4all/).

## Release Managers

The [release-tools repository](https://gitlab.com/gitlab-org/release-tools/tree/master) contains useful information about the responsibilities and tasks performed by a [release manager](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/release-manager.md). Here is the [upcoming and current list of release managers](/release-managers/).

Because of the volume of work required to get a release out the door, there will be two primary release managers:

1. One in the America time zones
2. One in Europe/Middle East/Africa (EMEA) or Asia Pacific (APAC)

Trained release managers, one in Americas and one on EMEA/APAC, will ultimately be in charge of making sure the release candidates (RCs) get created and deployed.

* These release managers need to be very vocal if they need help or something is blocking the release candidate (RC)

* Trainee release managers will do most of the hands-on work (e.g. cherry-picking, creating RCs, deploying, etc.).

* Trainers should allow trainees to do the work, but like a pilot of an airplane they can take over when time becomes critical.
