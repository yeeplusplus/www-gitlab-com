---
layout: markdown_page
title: "Board meetings"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Our process

1. The CFO is responsible for scheduling the meeting, preparing the agenda and recording minutes.
1. Collaborate on public webpages such as [/strategy](https://about.gitlab.com/strategy/) as much as possible.
1. Financial information and other non public items go into a shared Google Sheet and/or Google Presentation.
1. The CFO will send a reminder to those who are requested to prepare materials two weeks in advance of the meeting.
1. Final draft presentations are due one week prior to the meeting.
1. Board materials are distributed at least 48 hours before the meeting.
1. Discussion points are clearly marked, each get a time allotment, five minutes unless otherwise approved.
1. Board members are assumed to have studied the materials.
1. For now the whole executive team is present during the meeting.
1. There is a closed session at the end to cover administrative items and board only discussion.
1. No presentation during the meeting, only discussion items and conversation about unclear items.
1. Follow up with updated materials.

## Schedule
1. Board of Directors meetings are held quarterly and can be attended either in person or by videoconference.
1. Meetings are scheduled as close to the fourth thursday following the end of the quarter, depending on availability of the directors.
1. The 2018 schedule of board meetings is as follows:

	1. Q1 Meeting: 2018-01-25
	1. Q2 Meeting: 2018-04-24
	1. Q3 Meeting: 2018-07-26
	1. Q4 Meeting: 2018-11-01

EA shall ensure that there is one invite for all attendees that includes the following:
Exact meeting time blocked (ie: Start at 9am PST, End at 5pm PST)
Zoom Link
Agenda (the agenda should also include the zoom link at the top)

Make sure to:
Determine which participants will be attending remotely and in-person to ensure there is enough room.
Test Zoom set-up at least 1 hour before and at most 2 days before
Ensure remote participants feel invited and welcomed

## References

1. [AVC post](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/)
1. [AVC comment](http://avc.com/2016/02/do-you-want-better-board-meetings-then-work-the-phone/#comment-2489615046)
1. [Techcrunch article](http://techcrunch.com/2016/02/01/1270130/)
