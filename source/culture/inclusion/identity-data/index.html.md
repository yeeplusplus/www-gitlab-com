---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-02-28.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 235   | 100%        |
| Based in the US                           | 129   | 54.89%      |
| Based in the UK                           | 17    | 7.23%       |
| Based in the Netherlands                  | 10    | 4.26%       |
| Based in Other Countries                  | 79    | 33.62%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 235   | 100%        |
| Men                                       | 191   | 81.28%      |
| Women                                     | 44    | 18.72%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 26    | 100%        |
| Men in Leadership                         | 22    | 84.62%      |
| Women in Leadership                       | 4     | 15.38%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 106   | 100%        |
| Men in Development                        | 95    | 89.62%      |
| Women in Development                      | 11    | 10.38%      |
| Other Gender Options                      | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 129   | 100%        |
| Asian                                     | 10    | 7.75%       |
| Black or African American                 | 2     | 1.55%       |
| Hispanic or Latino                        | 6     | 4.65%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.78%       |
| Two or More Races                         | 2     | 1.55%       |
| White                                     | 72    | 55.04%      |
| Unreported                                | 37    | 28.68%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 27    | 100%        |
| Asian                                     | 4     | 14.81%      |
| Black or African American                 | 1     | 3.70%       |
| Two or More Races                         | 1     | 3.70%       |
| White                                     | 15    | 55.56%      |
| Unreported                                | 6     | 22.22%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 22    | 100%        |
| Asian                                     | 2     | 9.09%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.55%       |
| White                                     | 12    | 54.55%      |
| Unreported                                | 7     | 31.82%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 235   | 100%        |
| Asian                                     | 15    | 6.38%       |
| Black or African American                 | 5     | 2.13%       |
| Hispanic or Latino                        | 12    | 5.11%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.43%       |
| Two or More Races                         | 3     | 1.28%       |
| White                                     | 121   | 51.49%      |
| Unreported                                | 78    | 33.19%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 106   | 100%        |
| Asian                                     | 7     | 6.60%       |
| Black or African American                 | 3     | 2.83%       |
| Hispanic or Latino                        | 5     | 4.72%       |
| Two or More Races                         | 2     | 1.89%       |
| White                                     | 52    | 49.06%      |
| Unreported                                | 37    | 34.91%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 26    | 100%        |
| Asian                                     | 2     | 7.69%       |
| Native Hawaiian or Other Pacific Islander | 1     | 3.85%       |
| White                                     | 13    | 50.00%      |
| Unreported                                | 10    | 38.46%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 235   | 100%        |
| 20-24                                     | 15    | 6.38%       |
| 25-29                                     | 55    | 23.40%      |
| 30-34                                     | 64    | 27.23%      |
| 35-39                                     | 36    | 15.32%      |
| 40-49                                     | 41    | 17.45%      |
| 50-59                                     | 21    | 8.94%       |
| 60+                                       | 1     | 0.43%       |
| Unreported                                | 2     | 0.85%       |
